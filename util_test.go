package web

import (
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestByteSizeToFriendlyString(t *testing.T) {
	testCases := []struct {
		given    int64
		expected string
	}{
		{given: 1, expected: "1.00 B"},
		{given: 1023, expected: "1023.00 B"},
		{given: 1024, expected: "1.00 kB"},
		{given: 1536, expected: "1.50 kB"},
		{given: 1048576, expected: "1.00 MB"},
		{given: 3214822145, expected: "2.99 GB"},
	}

	for _, testCase := range testCases {
		actual := ByteSizeToFriendlyString(testCase.given)
		require.Equal(t, testCase.expected, actual)
	}
}

func TestUnmarshalFromResponseRecorder(t *testing.T) {
	// Arrange.
	m := &struct{ Name string }{}
	w := httptest.NewRecorder()
	w.Write([]byte(`{"Name":"John Smith"}`))

	// Act.
	err := UnmarshalFromResponseRecorder(w, m)

	// Assert.
	require.Nil(t, err)
	require.Equal(t, "John Smith", m.Name)
}
