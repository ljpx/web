package web

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/ljpx/di"
	"gitlab.com/ljpx/log"
	"gitlab.com/ljpx/trace"
)

type ContextSuite struct {
	suite.Suite

	w      *httptest.ResponseRecorder
	r      *http.Request
	c      di.Container
	logger log.Logger

	x *Context
}

func TestContextSuite(t *testing.T) {
	suite.Run(t, &ContextSuite{})
}

func (suite *ContextSuite) SetupTest() {
	suite.w = httptest.NewRecorder()
	suite.r = httptest.NewRequest(http.MethodGet, "/", nil)
	suite.c = di.NewContainer()
	suite.logger = log.NewDummy()

	suite.x = NewContext(suite.w, suite.r, suite.c, suite.logger)
	suite.x.SetDebuggingEnabled(true)
	suite.x.SetProblemPrefix("https://testi.ng")
	suite.x.SetJSONContentLengthLimit(1 << 20)
}

func (suite *ContextSuite) TestGeneratesCorrelationID() {
	// Arrange and Act.
	correlationID := suite.x.GetCorrelationID()

	// Assert.
	suite.Require().True(correlationID.IsValid())
}

func (suite *ContextSuite) TestGetSetMiddlewareArtifact() {
	// Arrange.
	suite.x.SetMiddlewareArtifact("test", "Hello, World!")

	// Act.
	artifact, ok := suite.x.GetMiddlewareArtifact("test").(string)

	// Assert.
	suite.Require().True(ok)
	suite.Require().Equal("Hello, World!", artifact)
}

func (suite *ContextSuite) TestGetMiddlewareArtifactSuccess() {
	// Arrange.
	suite.x.middlewareArtifacts["count"] = 5

	// Act.
	artifact, ok := suite.x.GetMiddlewareArtifact("count").(int)

	// Assert.
	suite.Require().True(ok)
	suite.Require().Equal(5, artifact)
}

func (suite *ContextSuite) TestGetMiddlewareArtifactFailure1() {
	// Arrange.
	suite.x.middlewareArtifacts["count"] = 5

	// Act.
	artifact, ok := suite.x.GetMiddlewareArtifact("counts").(int)

	// Assert.
	suite.Require().False(ok)
	suite.Require().Equal(0, artifact)
}

func (suite *ContextSuite) TestGetMiddlewareArtifactFailure2() {
	// Arrange.
	suite.x.middlewareArtifacts["count"] = "5"

	// Act.
	artifact, ok := suite.x.GetMiddlewareArtifact("count").(int)

	// Assert.
	suite.Require().False(ok)
	suite.Require().Equal(0, artifact)
}

func (suite *ContextSuite) TestRespondWithJSONSuccess() {
	// Arrange.
	model := &responseModel{Name: "John"}

	// Act.
	suite.x.RespondWithJSON(http.StatusOK, model)

	// Assert.
	suite.Require().Equal(http.StatusOK, suite.w.Code)

	response := suite.w.Result()
	defer response.Body.Close()

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"name":"John"}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestRespondWithJSONFailure() {
	// Arrange.
	model := &unmarshallable{}

	// Act.
	suite.x.RespondWithJSON(http.StatusOK, model)

	// Assert.
	suite.Require().Equal(http.StatusInternalServerError, suite.w.Code)

	response := suite.w.Result()
	defer response.Body.Close()

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/internal-server-error","title":"Internal Server Error","detail":"Serialization of the response model failed.","error":"json: error calling MarshalJSON for type *web.unmarshallable: cannot be marshalled"}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestAssertContentTypeSuccess() {
	// Arrange.
	suite.r.Header.Set("Content-Type", "image/png")

	// Act.
	passed := suite.x.AssertContentType("image/PNG")

	// Assert.
	suite.Require().True(passed)
}

func (suite *ContextSuite) TestAssertContentTypeFailure() {
	// Arrange.
	suite.r.Header.Set("Content-Type", "image/jpeg")

	// Act.
	passed := suite.x.AssertContentType("image/PNG", "image/gif")

	// Assert.
	suite.Require().False(passed)

	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusUnsupportedMediaType, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/unsupported-media-type","title":"Unsupported Media Type","detail":"The Content-Type 'image/jpeg' is not supported by this endpoint.","specifics":{"allowedContentTypes":["image/PNG","image/gif"],"providedContentType":"image/jpeg"}}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestAssertContentLengthSuccess() {
	// Arrange.
	suite.r = httptest.NewRequest(http.MethodGet, "/", bytes.NewBufferString("Hello, World!"))
	suite.r.Header.Set("Content-Length", "13")
	suite.x.Request = suite.r

	// Act.
	passed := suite.x.AssertContentLength(13)

	// Assert.
	suite.Require().True(passed)
}

func (suite *ContextSuite) TestAssertContentLengthFailureTooLarge() {
	// Arrange.
	suite.r = httptest.NewRequest(http.MethodGet, "/", bytes.NewBufferString("Hello, World!"))
	suite.r.Header.Set("Content-Length", "13")
	suite.x.Request = suite.r

	// Act.
	passed := suite.x.AssertContentLength(12)

	// Assert.
	suite.Require().False(passed)

	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusRequestEntityTooLarge, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/request-entity-too-large","title":"Request Entity Too Large","detail":"The provided request entity of length 13.00 B (13 bytes) exceeds the maximum of 12.00 B (12 bytes) on this endpoint.","specifics":{"contentLength":13,"maximumContentLength":12}}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestAssertContentLengthFailureNotProvided() {
	// Arrange and Act.
	passed := suite.x.AssertContentLength(12)

	// Assert.
	suite.Require().False(passed)

	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusLengthRequired, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/length-required","title":"Length Required","detail":"This endpoint requires that the Content-Length header be set to a positive, non-zero value."}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestFromJSONInvalidContentType() {
	// Arrange.
	suite.r = httptest.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`{"age":24}`))
	suite.r.Header.Set("Content-Length", "10")
	suite.r.Header.Set("Content-Type", "image/png")
	suite.x.Request = suite.r

	model := &requestModel{}

	// Act.
	passed := suite.x.FromJSON(model)

	// Assert.
	suite.Require().False(passed)
}

func (suite *ContextSuite) TestFromJSONNoContentLengthProvided() {
	// Arrange.
	suite.r.Header.Set("Content-Type", "application/json")

	model := &requestModel{}

	// Act.
	passed := suite.x.FromJSON(model)

	// Assert.
	suite.Require().False(passed)
}

func (suite *ContextSuite) TestFromJSONInvalidJSON() {
	// Arrange.
	suite.r = httptest.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`hellohello`))
	suite.r.Header.Set("Content-Length", "10")
	suite.r.Header.Set("Content-Type", "application/json")
	suite.x.Request = suite.r

	model := &requestModel{}

	// Act.
	passed := suite.x.FromJSON(model)

	// Assert.
	suite.Require().False(passed)

	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusBadRequest, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/bad-request","title":"Bad Request","detail":"The provided request body could not be meaningfully deserialized.  It appears to be invalid.","error":"invalid character 'h' looking for beginning of value"}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestFromJSONPurificationFailure() {
	// Arrange.
	suite.r = httptest.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`{"age":0}`))
	suite.r.Header.Set("Content-Length", "9")
	suite.r.Header.Set("Content-Type", "application/json")
	suite.x.Request = suite.r

	model := &requestModel{}

	// Act.
	passed := suite.x.FromJSON(model)

	// Assert.
	suite.Require().False(passed)

	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusUnprocessableEntity, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/unprocessable-entity","title":"Unprocessable Entity","detail":"The provided request body was understood but contained some invalid values.","specifics":{"error":"must be larger than 0","field":"age"}}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestFromJSONSuccess() {
	// Arrange.
	suite.r = httptest.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`{"age":24}`))
	suite.r.Header.Set("Content-Length", "10")
	suite.r.Header.Set("Content-Type", "application/json")
	suite.x.Request = suite.r

	model := &requestModel{}

	// Act.
	passed := suite.x.FromJSON(model)

	// Assert.
	suite.Require().True(passed)
}

func (suite *ContextSuite) TestAssertMethodSuccess() {
	// Arrange and Act.
	passed := suite.x.AssertMethod(http.MethodPost, http.MethodGet)

	// Assert.
	suite.Require().True(passed)
}

func (suite *ContextSuite) TestAssertMethodFailure() {
	// Arrange and Act.
	passed := suite.x.AssertMethod(http.MethodPost, http.MethodPut)

	// Assert.
	suite.Require().False(passed)

	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusMethodNotAllowed, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/method-not-allowed","title":"Method Not Allowed","detail":"This endpoint does not allow use of the 'GET' method.","specifics":{"allowedMethods":["POST","PUT"],"methodUsed":"GET"}}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestNotFound() {
	// Arrange and Act.
	suite.x.NotFound("User", "1234")

	// Assert.
	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusNotFound, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/not-found","title":"Not Found","detail":"The User '1234' was not found.","specifics":{"subject":"1234","subjectType":"User"}}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestUnauthorized() {
	// Arrange and Act.
	suite.x.Unauthorized("Bearer token not provided.")

	// Assert.
	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusUnauthorized, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/unauthorized","title":"Unauthorized","detail":"The request could not be authorized.","specifics":{"reason":"Bearer token not provided."}}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

func (suite *ContextSuite) TestInternalServerError() {
	// Arrange and Act.
	suite.x.InternalServerError(fmt.Errorf("ahhh"))

	// Assert.
	response := suite.w.Result()
	defer response.Body.Close()

	suite.Require().Equal(http.StatusInternalServerError, suite.w.Code)

	rawJSON, err := ioutil.ReadAll(response.Body)
	suite.Require().Nil(err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"request":"%v"},"type":"https://testi.ng/http/internal-server-error","title":"Internal Server Error","detail":"An internal server error prevented the request from completing.","error":"ahhh"}`, suite.x.correlationID)
	suite.Require().Equal(expectedJSON, json)
}

// -----------------------------------------------------------------------------

type responseModel struct {
	trace.CorrelatableBase

	Name string `json:"name"`
}

var _ trace.Redactable = &responseModel{}

func (*responseModel) Identify() string {
	return "ResponseModel"
}

func (*responseModel) Describe() string {
	return "This is a response model."
}

func (m *responseModel) Redact() interface{} {
	cp := *m
	return &cp
}

type requestModel struct {
	trace.CorrelatableBase

	Age int `json:"age"`
}

var _ Purifiable = &requestModel{}

func (*requestModel) Identify() string {
	return "RequestModel"
}

func (*requestModel) Describe() string {
	return "This is a request model."
}

func (m *requestModel) Purify() (string, error) {
	if m.Age == 0 {
		return "age", errors.New("must be larger than 0")
	}

	return "", nil
}

func (m *requestModel) Redact() interface{} {
	cp := *m
	return &cp
}

type unmarshallable struct {
	trace.CorrelatableBase
}

var _ json.Marshaler = &unmarshallable{}
var _ trace.Traceable = &unmarshallable{}

func (*unmarshallable) MarshalJSON() ([]byte, error) {
	return nil, errors.New("cannot be marshalled")
}

func (*unmarshallable) Identify() string {
	return "Unmarshallable"
}

func (*unmarshallable) Describe() string {
	return "cannot be marshalled"
}

func (m *unmarshallable) Redact() interface{} {
	cp := *m
	return &cp
}
