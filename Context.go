package web

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/ljpx/di"
	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/log"
	"gitlab.com/ljpx/problem"
	"gitlab.com/ljpx/trace"
)

// Context contains contextual items for a single HTTP request as well as
// defines utility methods for interacting with these contextual items.
type Context struct {
	w      http.ResponseWriter
	c      di.Container
	logger log.Logger

	Request *http.Request

	debuggingEnabled       bool
	problemPrefix          string
	jsonContentLengthLimit int64

	correlationID       id.ID
	middlewareArtifacts map[string]interface{}
}

// NewContext creates a new context for the provided request, di.Container and
// logger.
func NewContext(w http.ResponseWriter, r *http.Request, c di.Container, logger log.Logger) *Context {
	return &Context{
		w:      w,
		c:      c,
		logger: logger,

		Request: r,

		jsonContentLengthLimit: 1 << 20,

		correlationID:       id.New(),
		middlewareArtifacts: make(map[string]interface{}),
	}
}

// SetDebuggingEnabled allows errors to pass through into problems produced by
// this context.
func (ctx *Context) SetDebuggingEnabled(enabled bool) {
	ctx.debuggingEnabled = enabled
}

// SetProblemPrefix sets the URL prefix for problem details types produced by
// this context.
func (ctx *Context) SetProblemPrefix(prefix string) {
	ctx.problemPrefix = prefix
}

// SetJSONContentLengthLimit sets the JSON content length limit for use in the
// FromJSON method.  By default, this value is 1 << 20.
func (ctx *Context) SetJSONContentLengthLimit(limit int64) {
	ctx.jsonContentLengthLimit = limit
}

// GetCorrelationID returns the correlation ID for this request.
func (ctx *Context) GetCorrelationID() id.ID {
	return ctx.correlationID
}

// GetMiddlewareArtifact returns the middleware artifact with the specified
// name.  It will return nil if the artifact does not exist.
func (ctx *Context) GetMiddlewareArtifact(name string) interface{} {
	v, _ := ctx.middlewareArtifacts[name]
	return v
}

// SetMiddlewareArtifact sets the middleware artifact for the provided name.
func (ctx *Context) SetMiddlewareArtifact(name string, value interface{}) {
	ctx.middlewareArtifacts[name] = value
}

// Resolve calls Resolve on the underlying container.
func (ctx *Context) Resolve(into interface{}) {
	ctx.c.Resolve(into)
}

// RespondWithJSON replies to the provided request with the provided traceable
// model.
func (ctx *Context) RespondWithJSON(code int, model trace.Traceable) {
	model.AddCorrelation("request", ctx.correlationID)

	rawJSON, err := json.Marshal(model)
	if err != nil {
		rawJSON = []byte(ctx.problemForSerializationJSON(err))
		code = http.StatusInternalServerError
	}

	ctx.w.Header().Set("Content-Type", "application/json")
	ctx.w.Header().Set("Content-Length", fmt.Sprintf("%v", len(rawJSON)))
	ctx.Respond(code)
	ctx.w.Write([]byte(rawJSON))

	ctx.logger.Trace(model)
}

// Respond sets appropriate outgoing headers and the status code.
func (ctx *Context) Respond(code int) {
	ctx.w.Header().Set("X-Request-Correlation-ID", ctx.correlationID.String())
	ctx.w.WriteHeader(code)
}

// Header gets the header set of the response.
func (ctx *Context) Header() http.Header {
	return ctx.w.Header()
}

// Write writes to the underlying response writer.
func (ctx *Context) Write(b []byte) (int, error) {
	return ctx.w.Write(b)
}

// FromJSON retrieves JSON from the request body to place into the provided
// Purifiable.
func (ctx *Context) FromJSON(model Purifiable) bool {
	if !ctx.AssertContentType("application/json") {
		return false
	}

	if !ctx.AssertContentLength(ctx.jsonContentLengthLimit) {
		return false
	}

	decoder := json.NewDecoder(ctx.Request.Body)
	err := decoder.Decode(model)
	if err != nil {
		problem := ctx.problemForDeserialization(err)
		ctx.RespondWithJSON(http.StatusBadRequest, problem)
		return false
	}

	model.EmptyCorrelations()

	field, err := model.Purify()
	if err != nil {
		problem := ctx.problemForUnprocessableEntity(field, err)
		ctx.RespondWithJSON(http.StatusUnprocessableEntity, problem)
		return false
	}

	model.AddCorrelation("request", ctx.correlationID)
	ctx.logger.Trace(model)

	return true
}

// GetPathParameter retrieves a path segment parameter from the request.
func (ctx *Context) GetPathParameter(name string) string {
	val, _ := mux.Vars(ctx.Request)[name]
	return val
}

// GetQueryParameter retrieves a query parameter from the request.
func (ctx *Context) GetQueryParameter(name string) string {
	return ctx.Request.URL.Query().Get(name)
}

// NotFound replies with a 404 Not Found problem details.
func (ctx *Context) NotFound(subjectType string, subject string) {
	problem := ctx.problemForNotFound(subjectType, subject)
	ctx.RespondWithJSON(http.StatusNotFound, problem)
}

// Unauthorized replies with a 401 Unauthorized problem details.
func (ctx *Context) Unauthorized(reason string) {
	problem := ctx.problemForUnauthorized(reason)
	ctx.RespondWithJSON(http.StatusUnauthorized, problem)
}

// InternalServerError replies with a 500 Internal Server Error problem details.
func (ctx *Context) InternalServerError(err error) {
	problem := ctx.problemForInternalServerError(err)
	ctx.RespondWithJSON(http.StatusInternalServerError, problem)
}

// AssertContentType ensures that the content type of the request matches one of
// the content types provided.
func (ctx *Context) AssertContentType(allowedContentTypes ...string) bool {
	contentType := ctx.Request.Header.Get("Content-Type")
	contentTypeUppercase := strings.ToUpper(contentType)

	for _, allowedContentType := range allowedContentTypes {
		if contentTypeUppercase == strings.ToUpper(allowedContentType) {
			return true
		}
	}

	problem := ctx.problemForUnsupportedMediaType(contentType, allowedContentTypes)
	ctx.RespondWithJSON(http.StatusUnsupportedMediaType, problem)

	return false
}

// AssertContentLength ensures that a content length was provided, and that it
// is in (0, max].
func (ctx *Context) AssertContentLength(max int64) bool {
	contentLength := ctx.Request.ContentLength

	if contentLength > max {
		problem := ctx.problemForRequestEntityTooLarge(contentLength, max)
		ctx.RespondWithJSON(http.StatusRequestEntityTooLarge, problem)
		return false
	}

	if contentLength <= 0 {
		problem := ctx.problemForLengthRequired()
		ctx.RespondWithJSON(http.StatusLengthRequired, problem)
		return false
	}

	return true
}

// AssertMethod ensures that the incoming request is using one of the provided
// methods.
func (ctx *Context) AssertMethod(allowedMethods ...string) bool {
	methodUpperCase := strings.ToUpper(ctx.Request.Method)

	for _, allowedMethod := range allowedMethods {
		if methodUpperCase == strings.ToUpper(allowedMethod) {
			return true
		}
	}

	problem := ctx.problemForMethodNotAllowed(ctx.Request.Method, allowedMethods)
	ctx.RespondWithJSON(http.StatusMethodNotAllowed, problem)

	return false
}

func (ctx *Context) problemForUnsupportedMediaType(providedContentType string, allowedContentTypes []string) *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/unsupported-media-type", ctx.problemPrefix),
		Title:  "Unsupported Media Type",
		Detail: fmt.Sprintf("The Content-Type '%v' is not supported by this endpoint.", providedContentType),
		Specifics: map[string]interface{}{
			"providedContentType": providedContentType,
			"allowedContentTypes": allowedContentTypes,
		},
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForRequestEntityTooLarge(contentLength, max int64) *problem.Details {
	detailFormat := "The provided request entity of length %v (%v bytes) exceeds the maximum of %v (%v bytes) on this endpoint."
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/request-entity-too-large", ctx.problemPrefix),
		Title:  "Request Entity Too Large",
		Detail: fmt.Sprintf(detailFormat, ByteSizeToFriendlyString(contentLength), contentLength, ByteSizeToFriendlyString(max), max),
		Specifics: map[string]interface{}{
			"contentLength":        contentLength,
			"maximumContentLength": max,
		},
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForLengthRequired() *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/length-required", ctx.problemPrefix),
		Title:  "Length Required",
		Detail: "This endpoint requires that the Content-Length header be set to a positive, non-zero value.",
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForMethodNotAllowed(method string, allowedMethods []string) *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/method-not-allowed", ctx.problemPrefix),
		Title:  "Method Not Allowed",
		Detail: fmt.Sprintf(`This endpoint does not allow use of the '%v' method.`, method),
		Specifics: map[string]interface{}{
			"methodUsed":     method,
			"allowedMethods": allowedMethods,
		},
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForUnprocessableEntity(field string, err error) *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/unprocessable-entity", ctx.problemPrefix),
		Title:  "Unprocessable Entity",
		Detail: fmt.Sprintf(`The provided request body was understood but contained some invalid values.`),
		Specifics: map[string]interface{}{
			"field": field,
			"error": err.Error(),
		},
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForNotFound(subjectType string, subject string) *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/not-found", ctx.problemPrefix),
		Title:  "Not Found",
		Detail: fmt.Sprintf(`The %v '%v' was not found.`, subjectType, subject),
		Specifics: map[string]interface{}{
			"subjectType": subjectType,
			"subject":     subject,
		},
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForUnauthorized(reason string) *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/unauthorized", ctx.problemPrefix),
		Title:  "Unauthorized",
		Detail: fmt.Sprintf(`The request could not be authorized.`),
		Specifics: map[string]interface{}{
			"reason": reason,
		},
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForInternalServerError(err error) *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/internal-server-error", ctx.problemPrefix),
		Title:  "Internal Server Error",
		Detail: fmt.Sprintf("An internal server error prevented the request from completing."),
	}

	if ctx.debuggingEnabled {
		problem.AttachError(err)
	}

	problem.AddCorrelation("request", ctx.correlationID)
	return problem
}

func (ctx *Context) problemForDeserialization(err error) *problem.Details {
	problem := &problem.Details{
		Type:   fmt.Sprintf("%v/http/bad-request", ctx.problemPrefix),
		Title:  "Bad Request",
		Detail: "The provided request body could not be meaningfully deserialized.  It appears to be invalid.",
	}

	problem.AddCorrelation("request", ctx.correlationID)
	if ctx.debuggingEnabled {
		problem.AttachError(err)
	}

	return problem
}

func (ctx *Context) problemForSerializationJSON(err error) string {
	errStr := ""
	if ctx.debuggingEnabled && err != nil {
		errStr = fmt.Sprintf(`,"error":"%v"`, err.Error())
	}

	formatJSON := `{"correlations":{"request":"%v"},"type":"%v/http/internal-server-error","title":"Internal Server Error","detail":"Serialization of the response model failed."%v}`
	return fmt.Sprintf(formatJSON, ctx.correlationID, ctx.problemPrefix, errStr)
}
