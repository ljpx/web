package web

import "gitlab.com/ljpx/trace"

// Purifiable defines the methods that any purifiable request model must
// implement.  The intention is to allow request methods to be able to validate
// themselves - keeping the validation of user provided information out of
// routes.  Purifiable models are implicitly trace.Redactable.
//
// The first return value is the name of the invalid field, the second is the
// error describing the problem.
type Purifiable interface {
	trace.Traceable

	Purify() (string, error)
}
