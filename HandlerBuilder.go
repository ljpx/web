package web

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/ljpx/di"
	"gitlab.com/ljpx/log"
)

// HandlerBuilder is used to build a handler that can be passed to any HTTP
// server.  Once Build() has been called, the HandlerBuilder is invalid and can
// no longer be used.  HandlerBuilder is not thread safe.
type HandlerBuilder struct {
	c      di.Container
	logger log.Logger

	debuggingEnabled       bool
	problemPrefix          string
	jsonContentLengthLimit int64

	routesByPath map[string][]Route
	hasBeenBuilt bool
}

// NewHandlerBuilder creates a new HandlerBuilder with the provided container
// and logger.
func NewHandlerBuilder(c di.Container, logger log.Logger) *HandlerBuilder {
	return &HandlerBuilder{
		c:      c,
		logger: logger,

		jsonContentLengthLimit: 1 << 20,

		routesByPath: make(map[string][]Route),
	}
}

// SetDebuggingEnabled sets whether or not debugging is enabled.
func (hb *HandlerBuilder) SetDebuggingEnabled(debuggingEnabled bool) {
	hb.assertHasNotBeenBuilt()
	hb.debuggingEnabled = debuggingEnabled
}

// SetProblemPrefix sets the problem details prefix.
func (hb *HandlerBuilder) SetProblemPrefix(problemPrefix string) {
	hb.assertHasNotBeenBuilt()
	hb.problemPrefix = problemPrefix
}

// SetJSONContentLengthLimit sets the JSON content length limit for incoming
// JSON.
func (hb *HandlerBuilder) SetJSONContentLengthLimit(jsonContentLengthLimit int64) {
	hb.assertHasNotBeenBuilt()
	hb.jsonContentLengthLimit = jsonContentLengthLimit
}

// Use adds a route to the list of routes this handler should expose.
func (hb *HandlerBuilder) Use(route Route) {
	hb.assertHasNotBeenBuilt()

	path := strings.TrimSpace(strings.ReplaceAll(route.Path(), "\\", "/"))
	routeList, _ := hb.routesByPath[path]
	routeList = append(routeList, route)
	hb.routesByPath[path] = routeList
}

// Build a computed handler for the current settings and with the provided
// routes.
func (hb *HandlerBuilder) Build() http.Handler {
	hb.assertHasNotBeenBuilt()

	mx := mux.NewRouter()

	for path, routes := range hb.routesByPath {
		contextHandler := buildHandlerForPath(path, routes)
		mx.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
			mrw := NewMetricResponseWriter(w)
			ctx := hb.buildContext(mrw, r)

			defer func() {
				if p := recover(); p != nil {
					hb.logger.Panic(ctx.correlationID, p)

					err, ok := p.(error)
					if !ok {
						err = fmt.Errorf("%v", p)
					}

					if !mrw.HasWrittenHeaders() {
						ctx.InternalServerError(err)
						hb.logger.EndRequest(ctx.correlationID, mrw.StatusCode(), mrw.Volume(), mrw.Duration())
					}
				}
			}()

			hb.logger.BeginRequest(ctx.correlationID, r.Method, r.URL.RawPath, r.RemoteAddr)
			contextHandler(ctx)
			hb.logger.EndRequest(ctx.correlationID, mrw.StatusCode(), mrw.Volume(), mrw.Duration())
		})
	}

	mx.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := hb.buildContext(w, r)
		ctx.NotFound("Path", r.URL.Path)
	})

	hb.hasBeenBuilt = true
	return mx
}

func (hb *HandlerBuilder) assertHasNotBeenBuilt() {
	if hb.hasBeenBuilt {
		panic("cannot use HandlerBuilder that has already been used to build a handler")
	}
}

func (hb *HandlerBuilder) buildContext(w http.ResponseWriter, r *http.Request) *Context {
	ctx := NewContext(w, r, hb.c.Fork(), hb.logger)

	ctx.SetDebuggingEnabled(hb.debuggingEnabled)
	ctx.SetProblemPrefix(hb.problemPrefix)
	ctx.SetJSONContentLengthLimit(hb.jsonContentLengthLimit)

	return ctx
}

func buildHandlerForPath(path string, routes []Route) ContextHandlerFunc {
	handlerByMethod := make(map[string]ContextHandlerFunc)
	allowedMethods := []string{}

	for _, route := range routes {
		method := route.Method()

		handlerByMethod[method] = buildHandlerForRoute(route)
		allowedMethods = append(allowedMethods, method)
	}

	return func(ctx *Context) {
		if !ctx.AssertMethod(allowedMethods...) {
			return
		}

		handlerByMethod[ctx.Request.Method](ctx)
	}
}

func buildHandlerForRoute(route Route) ContextHandlerFunc {
	return func(ctx *Context) {
		for _, mw := range route.Middleware() {
			shouldContinue := mw.Handle(ctx)
			if !shouldContinue {
				return
			}
		}

		route.Handle(ctx)
	}
}
