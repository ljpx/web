package web

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/ljpx/di"
	"gitlab.com/ljpx/log"
	"gitlab.com/ljpx/problem"
	"gitlab.com/ljpx/trace"
)

type HandlerBuilderSuite struct {
	suite.Suite

	c      di.Container
	logger log.Logger

	x *HandlerBuilder
}

func TestHandlerBuilderSuite(t *testing.T) {
	suite.Run(t, &HandlerBuilderSuite{})
}

func (suite *HandlerBuilderSuite) SetupTest() {
	suite.c = di.NewContainer()
	suite.logger = log.NewDummy()

	suite.x = NewHandlerBuilder(suite.c, suite.logger)
	suite.x.SetProblemPrefix("https://testi.ng")
	suite.x.SetDebuggingEnabled(true)
	suite.x.SetJSONContentLengthLimit(1 << 20)

	suite.x.Use(&testRoute1{})
}

func (suite *HandlerBuilderSuite) Test404() {
	// Arrange.
	handler := suite.x.Build()

	// Act.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/hello?withQuery=1", nil)
	handler.ServeHTTP(w, r)

	// Assert.
	suite.Require().Equal(http.StatusNotFound, w.Code)

	problem := &problem.Details{}
	err := UnmarshalFromResponseRecorder(w, problem)
	suite.Require().Nil(err)

	suite.Require().Equal("https://testi.ng/http/not-found", problem.Type)
	suite.Require().Equal(`The Path '/hello' was not found.`, problem.Detail)
}

func (suite *HandlerBuilderSuite) TestRoute1MethodNotAllowed() {
	// Arrange.
	handler := suite.x.Build()

	// Act.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodDelete, "/testRoute1/4321", nil)
	handler.ServeHTTP(w, r)

	// Assert.
	suite.Require().Equal(http.StatusMethodNotAllowed, w.Code)
}

func (suite *HandlerBuilderSuite) TestRoute1Success() {
	// Arrange.
	handler := suite.x.Build()
	json := `{"name":"John Smith","age":24}`

	// Act.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, "/testRoute1/4321?color=red", bytes.NewBufferString(json))
	r.Header.Set("Content-Type", "application/json")
	r.Header.Set("Authorization", "password")
	handler.ServeHTTP(w, r)

	// Assert.
	suite.Require().Equal(http.StatusOK, w.Code)

	resModel := &testResponseModel1{}
	err := UnmarshalFromResponseRecorder(w, resModel)
	suite.Require().Nil(err)

	expectedMsg := "Hello John Smith, aged 24!  ID: 4321, Color: red"
	suite.Require().Equal(expectedMsg, resModel.Message)
}

func (suite *HandlerBuilderSuite) TestPanicHandler() {
	// Arrange.
	handler := suite.x.Build()
	json := `{"name":"John Smith","age":99}`

	// Act.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, "/testRoute1/4321?color=red", bytes.NewBufferString(json))
	r.Header.Set("Content-Type", "application/json")
	r.Header.Set("Authorization", "password")
	handler.ServeHTTP(w, r)

	// Assert.
	suite.Require().Equal(http.StatusInternalServerError, w.Code)

	resModel := &problem.Details{}
	err := UnmarshalFromResponseRecorder(w, resModel)
	suite.Require().Nil(err)

	expectedMsg := "something broke"
	suite.Require().Equal(expectedMsg, resModel.Error)
}

// -----------------------------------------------------------------------------

type testMiddleware1 struct{}

var _ Middleware = &testMiddleware1{}

func (*testMiddleware1) Handle(ctx *Context) bool {
	if ctx.Request.Header.Get("Authorization") != "password" {
		ctx.Unauthorized("Invalid Password")
		return false
	}

	ctx.SetMiddlewareArtifact("authed", true)

	return true
}

type testRoute1 struct{}

var _ Route = &testRoute1{}

func (*testRoute1) Method() string {
	return http.MethodPost
}

func (*testRoute1) Path() string {
	return "/testRoute1/{id}"
}

func (*testRoute1) Middleware() []Middleware {
	return []Middleware{
		&testMiddleware1{},
	}
}

func (*testRoute1) Handle(ctx *Context) {
	authed := ctx.GetMiddlewareArtifact("authed").(bool)
	if !authed {
		ctx.Unauthorized("middleware")
		return
	}

	reqModel := &testRequestModel1{}
	if !ctx.FromJSON(reqModel) {
		return
	}

	if reqModel.Age == 99 {
		panic("something broke")
	}

	id := ctx.GetPathParameter("id")
	color := ctx.GetQueryParameter("color")

	resModel := &testResponseModel1{}
	resModel.Message = fmt.Sprintf("Hello %v, aged %v!  ID: %v, Color: %v", reqModel.Name, reqModel.Age, id, color)

	ctx.RespondWithJSON(http.StatusOK, resModel)
}

type testRequestModel1 struct {
	trace.CorrelatableBase

	Name string `json:"name"`
	Age  uint   `json:"age"`
}

var _ Purifiable = &testRequestModel1{}

func (*testRequestModel1) Identify() string {
	return "testRequestModel1"
}

func (m *testRequestModel1) Describe() string {
	return fmt.Sprintf("%v aged %v", m.Name, m.Age)
}

func (m *testRequestModel1) Purify() (string, error) {
	return "", nil
}

func (m *testRequestModel1) Redact() interface{} {
	cp := *m
	return &cp
}

type testResponseModel1 struct {
	trace.CorrelatableBase

	Message string `json:"message"`
}

var _ trace.Traceable = &testResponseModel1{}

func (*testResponseModel1) Identify() string {
	return "testResponseModel1"
}

func (m *testResponseModel1) Describe() string {
	return fmt.Sprintf("testResponseModel1 message with size %v", ByteSizeToFriendlyString(int64(len(m.Message))))
}

func (m *testResponseModel1) Redact() interface{} {
	cp := *m
	return &cp
}
