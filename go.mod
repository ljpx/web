module gitlab.com/ljpx/web

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/stretchr/testify v1.4.0
	gitlab.com/ljpx/di v1.1.0
	gitlab.com/ljpx/id v1.0.0
	gitlab.com/ljpx/log v1.0.6
	gitlab.com/ljpx/problem v1.0.4
	gitlab.com/ljpx/trace v1.0.6
)
