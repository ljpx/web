![](icon.png)

# web

Package `web` provides a set of tools and abstractions to enable HTTP
applications to scale while maintaining readability.

## Usage Example

```go
// Ping :: GET /ping
type Ping struct{}

var _ web.Route = &Ping{}

// Method returns the route method.
func (*Ping) Method() string {
    return http.MethodGet
}

// Path returns the route path.
func (*Ping) Path() string {
    return "/ping"
}

// Middleware returns the middleware for the route.
func (*Ping) Middleware() []web.Middleware {
    return nil
}

// Handle is the handler for the route.
func (*Ping) Handle(ctx *web.Context) {
    responseModel := &models.PingResponseModel{
        Now: time.Now(),
    }

    ctx.RespondWithJSON(http.StatusOK, responseModel)
}
```
